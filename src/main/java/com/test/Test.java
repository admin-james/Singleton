package com.test;

import com.util.ConfigManager;
import com.util.ConfigManagerHungry;
import com.util.ConfigManagerIdler;

/**
 * @author Simon
 */
public class Test {
    public static void main(String[] args) {
        System.out.println("one  -----------------");
        System.out.println(getConfigManager());
        System.out.println(getConfigManagerHungry());
        System.out.println(getConfigManagerIdler());
        System.out.println("two  -----------------");
        System.out.println(getConfigManager());
        System.out.println(getConfigManagerHungry());
        System.out.println(getConfigManagerIdler());
    }

    /**
     * 单例模式
     */
    private static String getConfigManager() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("username : " + ConfigManager.getConfigManager().getValue("username") + "\n");
        stringBuffer.append("password : " + ConfigManager.getConfigManager().getValue("password") + "\n");
        return stringBuffer.toString();
    }

    /**
     * 饿汉模式
     */
    private static String getConfigManagerHungry() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("username : " + ConfigManagerHungry.getConfigManagerHungry().getValue("username") + "\n");
        stringBuffer.append("password : " + ConfigManagerHungry.getConfigManagerHungry().getValue("password") + "\n");
        return stringBuffer.toString();
    }

    /**
     * 懒汉模式
     */
    private static String getConfigManagerIdler() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("username : " + ConfigManagerIdler.getConfigManagerIdler().getValue("username") + "\n");
        stringBuffer.append("password : " + ConfigManagerIdler.getConfigManagerIdler().getValue("password") + "\n");
        return stringBuffer.toString();
    }
}
