package com.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 单例 饿汉模式
 * 类初始化自行实例化,不存在线程不安全
 * 不具备延迟加载特性
 */
public class ConfigManagerHungry {
    /**
     * 类加载自行进行初始化操作
     */
    private static ConfigManagerHungry configManagerHungry = new ConfigManagerHungry();
    private static Properties properties;

    /**
     * 私有构造器 读取数据库配置文件
     */
    private ConfigManagerHungry() {
        System.out.println("饿汉模式 ConfigManagerHungry ------>>>> 创建");
        String configFile = "database.properties";
        properties = new Properties();
        InputStream is = ConfigManagerHungry.class.getClassLoader().getResourceAsStream(configFile);
        try {
            properties.load(is);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 全局访问点
     * @return
     */
    public static ConfigManagerHungry getConfigManagerHungry() {
        return configManagerHungry;
    }

    public String getValue(String key) {
        return properties.getProperty(key);
    }
}
