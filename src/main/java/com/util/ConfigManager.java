package com.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 单例模式
 * 结合饿汉模式与懒汉模式的特性
 * 及具有延迟加载特性并线程安全
 *
 * @author Simon
 */
public class ConfigManager {
    private static ConfigManager configManager;
    private static Properties properties;

    /**
     * 私有构造器 读取数据库配置文件
     */
    private ConfigManager() {
        String configFile = "database.properties";
        properties = new Properties();
        InputStream is = ConfigManager.class.getClassLoader().getResourceAsStream(configFile);
        try {
            properties.load(is);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 使用内部静态类
     */
    private static class ConfigManagerHelper {
        private static final ConfigManager INSTANCE = new ConfigManager();
    }

    /**
     * 全局访问点
     *
     * @return
     */
    public static ConfigManager getConfigManager() {
        if (null == configManager) {
            System.out.println("单例模式 ConfigManager ------>>>> 创建");
            configManager = ConfigManagerHelper.INSTANCE;
        }
        return configManager;
    }

    public String getValue(String key) {
        return properties.getProperty(key);
    }
}
