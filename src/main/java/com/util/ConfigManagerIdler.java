package com.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 单例 懒汉模式
 * 具备延迟加载特性
 * 线程不安全
 * @author Simon
 */
public class ConfigManagerIdler {
    private static ConfigManagerIdler configManagerIdler;
    private static Properties properties;

    /**
     * 私有构造器 读取数据库配置文件
     */
    private ConfigManagerIdler() {
        String configFile = "database.properties";
        properties = new Properties();
        InputStream is = ConfigManagerIdler.class.getClassLoader().getResourceAsStream(configFile);
        try {
            properties.load(is);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //全局访问点
    public static ConfigManagerIdler getConfigManagerIdler() {
        if (null == configManagerIdler) {
            System.out.println("懒汉模式 ConfigManagerIdler ------>>>> 创建");
            configManagerIdler = new ConfigManagerIdler();
        }
        return configManagerIdler;
    }

    public String getValue(String key) {
        return properties.getProperty(key);
    }
}
